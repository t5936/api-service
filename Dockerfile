FROM golang:1.15 AS builder

RUN apt-get update

RUN apt-get install -y protobuf-compiler

# Set GOPATH to build Go app
ENV GOPATH=/go

# Set apps source directory
ENV SRC_DIR=${GOPATH}/src

# Define current working directory
WORKDIR ${SRC_DIR}

# Copy apps scource code to the image
COPY . ${SRC_DIR}

# Build App
RUN ./build.sh

EXPOSE 8080

CMD apiService serve
